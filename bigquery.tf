# Enable the BigQuery API
resource "google_project_service" "bigquery" {
  project = "${google_project.demo.project_id}"
  service = "bigquery-json.googleapis.com"
}

# Create a BigQuery dataset to use for demo
resource "google_bigquery_dataset" "telemetry" {
  dataset_id    = "telemetry"
  project       = "${google_project.demo.project_id}"
  friendly_name = "Telemetry data for demo"
  description   = "Telemetry and supporting data received from telemetry application"
  location      = "US"
}

# Add the demo group as BigQuery users
resource "google_project_iam_member" "bigquery" {
  project = "${google_project.demo.project_id}"
  role    = "roles/bigquery.user"
  member  = "group:${var.editors_group}"
}
