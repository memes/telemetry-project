# Enable Dataflow API
resource "google_project_service" "dataflow" {
  project = "${google_project.demo.project_id}"
  service = "dataflow.googleapis.com"
}

/*
resource "google_dataflow_job" "telemetry_flow" {
  name              = "telemetry-flow"
  project           = "${google_project.demo.project_id}"
  template_gcs_path = "${google_storage_bucket.bucket.url}/dataflow/templates/telemetry_flow"
  temp_gcs_location = "${google_storage_bucket.bucket.url}/dataflow/tmp"
  zone              = "${var.zone}"

  parameters {
    subscription  = "${google_pubsub_subscription.streaming.path}"
    targetDataset = "${google_bigquery_dataset.telemetry.dataset_id}"
    targetProject = "${google_project.demo.project_id}"
    tempLocation  = "${google_storage_bucket.bucket.url}/dataflow/tmp"
  }
}
*/

