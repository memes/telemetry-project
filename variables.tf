# Assume the project will be part of Neudesic org
variable "org_domain_name" {
  default = "neudesic.com"
}

# Use the defualt Neudesic billing account
variable "org_billing_name" {
  default = "neugcp"
}

# Default project name is telemetry-demo
variable "project_name" {
  default = "telemetry-demo"
}

# Default to us-central1 for breadth of services
variable "region" {
  default = "us-central1"
}

# Default zone to use
variable "zone" {
  default = "us-central1f"
}

# Location to use for GAE
variable "gae_location" {
  default = "us-central"
}

# The project will use this domain as root for services
variable "domain_name" {
  default = "telemetrydemo.com"
}

# The user-group that contains people with Edit access to the project
variable "editors_group" {
  default = "gcpdemos@neudesic.com"
}
