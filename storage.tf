# Enable Cloud Storage APIs
resource "google_project_service" "storage-api" {
  project = "${google_project.demo.project_id}"
  service = "storage-api.googleapis.com"
}

resource "google_project_service" "storage-component" {
  project = "${google_project.demo.project_id}"
  service = "storage-component.googleapis.com"
}

resource "random_id" "bucket" {
  byte_length = 4
}

# Create a bucket for the project to use
resource "google_storage_bucket" "bucket" {
  name          = "${var.project_name}-${random_id.bucket.hex}"
  location      = "${var.region}"
  storage_class = "REGIONAL"
  project       = "${google_project.demo.project_id}"
}

output "bucket_url" {
  value = "${google_storage_bucket.bucket.url}"
}
