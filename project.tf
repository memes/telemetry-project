# Setup a project for telemetry demo

# Create a random token to uniquely identify project
resource "random_id" "project" {
  byte_length = 4
}

# Define a project resource for the demo
resource "google_project" "demo" {
  name            = "${var.project_name}"
  project_id      = "${var.project_name}-${random_id.project.hex}"
  org_id          = "${data.google_organization.org.id}"
  billing_account = "${data.google_billing_account.billing.id}"

  app_engine {
    location_id = "${var.gae_location}"
  }

  # This is key to avoiding problems with lack of GAE configs
  lifecycle {
    ignore_changes = ["app_engine"]
  }
}

output "project_id" {
  value = "${google_project.demo.project_id}"
}

# Add the editors-group to the project as editors
resource "google_project_iam_member" "editors" {
  project = "${google_project.demo.project_id}"
  role    = "roles/editor"
  member  = "group:${var.editors_group}"
}
