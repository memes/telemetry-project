# Set some defaults for GCP provider
provider "google" {
  region  = "${var.region}"
  version = "~> 1.14"
}

# Retreive a reference to the organization using the domain name of org
data "google_organization" "org" {
  domain = "${var.org_domain_name}"
}

# Get the billing details for the org
data "google_billing_account" "billing" {
  open         = true
  display_name = "${var.org_billing_name}"
}
