# Enable the DNS API
resource "google_project_service" "dns" {
  project = "${google_project.demo.project_id}"
  service = "dns.googleapis.com"
}

# Define the DNS zone
resource "google_dns_managed_zone" "root" {
  name        = "${var.project_name}-root"
  dns_name    = "${var.domain_name}."
  description = "Root DNS zone for ${var.project_name}"
  project     = "${google_project.demo.project_id}"
}

output "nameservers" {
  value = "${google_dns_managed_zone.root.name_servers}"
}

/* Domain verification record - must be present for AppEngine and SSL
 * NOTE: use of CNAME is recommended for AppEngine
 */
resource "google_dns_record_set" "domain-verification" {
  name         = "noyxpeodp5h3.${google_dns_managed_zone.root.dns_name}"
  project      = "${google_project.demo.project_id}"
  type         = "CNAME"
  ttl          = 3600
  managed_zone = "${google_dns_managed_zone.root.name}"
  rrdatas      = ["gv-zwcyvh7atl5qj3.dv.googlehosted.com."]
}

# App Engine records for main domain
resource "google_dns_record_set" "default_gae_ipv4" {
  name         = "${google_dns_managed_zone.root.dns_name}"
  project      = "${google_project.demo.project_id}"
  type         = "A"
  ttl          = 300
  managed_zone = "${google_dns_managed_zone.root.name}"
  rrdatas      = ["216.239.32.21", "216.239.34.21", "216.239.36.21", "216.239.38.21"]
}

resource "google_dns_record_set" "default_gae_ipv6" {
  name         = "${google_dns_managed_zone.root.dns_name}"
  project      = "${google_project.demo.project_id}"
  type         = "AAAA"
  ttl          = 300
  managed_zone = "${google_dns_managed_zone.root.name}"
  rrdatas      = ["2001:4860:4802:32::15", "2001:4860:4802:34::15", "2001:4860:4802:36::15", "2001:4860:4802:38::15"]
}
