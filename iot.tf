# Enable Cloud IoT Core APIs
resource "google_project_service" "cloudiot" {
  project = "${google_project.demo.project_id}"
  service = "cloudiot.googleapis.com"
}

resource "google_cloudiot_registry" "telemetry" {
  name    = "telemetry"
  project = "${google_project.demo.project_id}"
  region  = "${var.region}"

  event_notification_config = {
    pubsub_topic_name = "${google_pubsub_topic.telemetry.id}"
  }

  state_notification_config = {
    pubsub_topic_name = "${google_pubsub_topic.state.id}"
  }

  http_config = {
    http_enabled_state = "HTTP_ENABLED"
  }

  mqtt_config = {
    mqtt_enabled_state = "MQTT_ENABLED"
  }

  credentials = [
    {
      public_key_certificate = {
        format      = "X509_CERTIFICATE_PEM"
        certificate = "${file("ca.pem")}"
      }
    },
  ]
}

output "registry-name" {
  value = "${google_cloudiot_registry.telemetry.name}"
}

output "registry-region" {
  value = "${google_cloudiot_registry.telemetry.region}"
}
