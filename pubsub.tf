# Enable Pub/Sub API for the project
resource "google_project_service" "pubsub" {
  project = "${google_project.demo.project_id}"
  service = "pubsub.googleapis.com"
}

# This topic will be used for telemetry data
resource "google_pubsub_topic" "telemetry" {
  name    = "telemetry-topic"
  project = "${google_project.demo.project_id}"
}

# This topic will be used for state data - currently unused in demo
resource "google_pubsub_topic" "state" {
  name    = "state-topic"
  project = "${google_project.demo.project_id}"
}

# Subscription to telemetry data for streaming processing
resource "google_pubsub_subscription" "streaming" {
  name                 = "streaming-sub"
  topic                = "${google_pubsub_topic.telemetry.name}"
  ack_deadline_seconds = 10
  project              = "${google_project.demo.project_id}"
}

output "streaming-telemetry-sub" {
  value = "${google_pubsub_subscription.streaming.path}"
}

# Subscription to telemetry data for real-time processing
resource "google_pubsub_subscription" "real-time" {
  name                 = "real-time-sub"
  topic                = "${google_pubsub_topic.telemetry.name}"
  ack_deadline_seconds = 10
  project              = "${google_project.demo.project_id}"
}

output "realtime-telemetry-sub" {
  value = "${google_pubsub_subscription.real-time.path}"
}
